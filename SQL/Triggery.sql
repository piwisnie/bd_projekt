-----------------------------------------------------------
--Trigger DodawanieRezerwacjiDK
-- dba o to, aby klient nie przekroczyl limitu miejsc na nia dk
-----------------------------------------------------------


CREATE TRIGGER DodawanieRezerwacjiDK ON RezerwacjaKonferencji
FOR INSERT, UPDATE
AS
BEGIN
		IF  (select COUNT(*) from inserted) > 1 
		BEGIN
				RAISERROR('Nie mozna zmieniac/dodawac wiecej niz jednej rezerwacji naraz. ', 16, 1)
				ROLLBACK TRANSACTION
		END
		Declare @id_DK int
		Declare @ilosc_miejsc int
		Declare @ilosc_potwierdzen int
		
		set @id_DK = (select inserted.id_DK from inserted)
		set @ilosc_miejsc = (select inserted.ilosc_miejsc from inserted)
		set @ilosc_potwierdzen = (select inserted.ilosc_potwierdzen from inserted)

		IF @ilosc_potwierdzen > @ilosc_miejsc
			BEGIN
				RAISERROR('Nie moze byc wiecej potwierdzen niz rezerwacji.', 16, 1)
				ROLLBACK TRANSACTION
			END
		
		IF (dbo.liczba_wolnych_miejsc_na_dk(@id_DK)<0)
			BEGIN
				RAISERROR('Nie ma miejsca na tyle rezerwacji.', 16, 1)
				ROLLBACK TRANSACTION
			END
		
END
go

-----------------------------------------------------------
--Trigger DodawanieRezerwacjiW
-- dba o to, aby klient nie przekroczyl limitu miejsc na nia 
-----------------------------------------------------------

CREATE TRIGGER DodawanieRezerwacjiW ON RezerwacjaWarsztat
FOR INSERT, UPDATE
AS
BEGIN
		IF  (select COUNT(*) from inserted) > 1 
		BEGIN
				RAISERROR('Nie mozna zmieniac/dodawac wiecej niz jednej rezerwacji naraz. ', 16, 1)
				ROLLBACK TRANSACTION
		END
		Declare @id_w int
		Declare @ilosc_miejsc int
		Declare @ilosc_potwierdzen int
		
		set @id_w = (select inserted.id_w from inserted)
		set @ilosc_miejsc = (select inserted.ilosc_miejsc from inserted)
		set @ilosc_potwierdzen = (select inserted.ilosc_potwierdzen from inserted)

		IF @ilosc_potwierdzen > @ilosc_miejsc
			BEGIN
				RAISERROR('Nie moze byc wiecej potwierdzen niz rezerwacji.', 16, 1)
				ROLLBACK TRANSACTION
			END
		
		IF dbo.liczba_wolnych_miejsc_na_warsztat(@id_w) < 0
			BEGIN
				RAISERROR('Nie ma miejsca na tyle rezerwacji.', 16, 1)
				ROLLBACK TRANSACTION
			END
		
END
go

-----------------------------------------------------------
--Trigger DodawanieRejestracjiiDK
-- dba o to, aby klient nie przekroczyl limitu miejsc na rejestracje
-----------------------------------------------------------

CREATE TRIGGER DodawanieRejestracjiDK ON RejestracjaKonferencji
FOR INSERT, UPDATE
AS
BEGIN
		IF  (select COUNT(*) from inserted) > 1 
		BEGIN
				RAISERROR('Nie mozna zmieniac/dodawac wiecej niz jednej rejestracji naraz. ', 16, 1)
				ROLLBACK TRANSACTION
		END
		Declare @id_rezKonferencja int
		Declare @id_uczestnika varchar(10)
		set @id_rezKonferencja = (select inserted.id_rezKonferencja from inserted)
		set @id_uczestnika = (select inserted.id_uczestnika from inserted)
		
		IF (select COUNT(*) from RejestracjaKonferencji as rejk
						   inner join RezerwacjaKonferencji as rezk on rezk.id_rezKonferencja=rejk.id_rezKonferencja
						   where rejk.id_uczestnika = @id_uczestnika
						   and rejk.id_rezKonferencja = @id_rezKonferencja)>1
			BEGIN
				RAISERROR('Taki uczestnik jest juz na tej konferencji.', 16, 1)
				ROLLBACK TRANSACTION
			END
		
		IF (dbo.liczba_wolnych_miejsc_na_rezDK(@id_rezKonferencja) < 0)
			BEGIN
				RAISERROR('Limit rejestracji wyczerpany.', 16, 1)
				ROLLBACK TRANSACTION
			END
		
END
go


-----------------------------------------------------------
--Trigger DodawanieRejestracjiW
-- dba o to, aby klient nie przekroczyl limitu miejsc na rejestracje
-----------------------------------------------------------

CREATE TRIGGER DodawanieRejestracjiW ON RejestracjaWarsztat
FOR INSERT, UPDATE
AS
BEGIN
		IF  (select COUNT(*) from inserted) > 1 
		BEGIN
				RAISERROR('Nie mozna zmieniac/dodawac wiecej niz jednej rejestracji naraz. ', 16, 1)
				ROLLBACK TRANSACTION
		END
		Declare @id_rezWarsztat int
		Declare @id_uczestnika varchar(10)
		Declare @id_warsztatu int

		set @id_rezWarsztat = (select inserted.id_rezWarsztat from inserted)
		set @id_uczestnika = (select inserted.id_uczestnika from inserted)
		set @id_warsztatu = (select id_warsztat from Warsztat where id_warsztat = (
								select id_w from RezerwacjaWarsztat where id_rezWarsztat = @id_rezWarsztat
							)) 

		IF not exists(select COUNT(*) from RejestracjaKonferencji as rejk
								inner join RezerwacjaKonferencji as rezk on rezk.id_rezKonferencja = rejk.id_rezKonferencja
								inner join DzienKonferencji as dk on dk.id_konferencja = rezk.id_DK
								inner join Warsztat as w on w.id_dk = dk.id_dk 
								where rejk.id_uczestnika = @id_uczestnika
								and w.id_warsztat = @id_warsztatu)
			BEGIN
				RAISERROR('Uczestnik nie jest zapisany na konferencja danego dnia.', 16, 1)
				ROLLBACK TRANSACTION
			END

		IF(dbo.kolizjaWarsztatu(@id_uczestnika, @id_warsztatu) = 1)
			BEGIN
				RAISERROR('Kolizja Warsztatow dla tego uczestnika.', 16, 1)
				ROLLBACK TRANSACTION
			END
	
		IF (dbo.liczba_wolnych_miejsc_na_rezW(@id_rezWarsztat) <= 0)
			BEGIN
				RAISERROR('Limit rejestracji wyczerpany.', 16, 1)
				ROLLBACK TRANSACTION
			END
		
	
END
go



-- =============================================
-- Author:		Piotr Wisniewski
-- Description: lista_osobow_na_dk
/*
zwraca liste osobowa uczestnikow na
dzien konferencji podanym jako parametr
*/
-- =============================================

GO
create function lista_osobowa_na_dk(@id_dk int) 
	returns table
	as
	return
		
		select * from Uczestnicy where id_uczestnika in (
			select id_uczestnika from RejestracjaKonferencji where id_rezKonferencja in(
				select id_rezKonferencja from RezerwacjaKonferencji where id_DK =(
					select id_dk from DzienKonferencji where id_dk = @id_dk
				) 
			)
		)
	go

-- =============================================
-- Author:		Piotr Wisniewski
-- Description: lista_osobow_na_w
/*
zwraca liste osobowa uczestnikow na
warsztat podanym jako parametr
*/
-- =============================================

GO
create function lista_osobowa_na_w(@id_w int) 
	returns table
	as
	return
		
		select * from Uczestnicy where id_uczestnika in (
			select id_uczestnika from RejestracjaWarsztat where id_rezWarsztat in(
				select id_rezWarsztat from RezerwacjaWarsztat where id_w =(
					select id_warsztat from Warsztat where id_warsztat = @id_w
				) 
			)
		)
	go


-- =============================================
-- Author:		PiotrWisniewski
-- Description:	koszt_warsztatu
--zwraca koszt rezerwacji warsztatu
--(parametry: id_rerwacji
--koszt: il_os_z_leg*(koszt warsztatu * 1-znizka) + il_os_bez_leg * koszt warsztatu)
-- =============================================

GO
create function koszt_warsztatu(@id_rezWarsztat int) 
	returns money 
	as
	begin
		declare @koszt as money
		declare @koszt_warsztatu as money
		declare @il_os_z_leg as int
		declare @il_os_bez_leg as int
		declare	@znizka as float 
		
		set @koszt_warsztatu =(
			select cena from Warsztat where id_warsztat = (
				select id_w from RezerwacjaWarsztat where id_rezWarsztat = @id_rezWarsztat
			))

		set @il_os_z_leg = (
			(
			select Count(*) from RejestracjaWarsztat where id_rezWarsztat =  @id_rezWarsztat
													   and legitymacja is not null 
			))
		set @il_os_bez_leg = (
			(
			select Count(*) from RejestracjaWarsztat where id_rezWarsztat = @id_rezWarsztat
													   and legitymacja is null 
			))
		
		set @znizka = (
			select znizka from Warsztat where id_warsztat = (
				select id_w from RezerwacjaWarsztat where id_rezWarsztat =  @id_rezWarsztat
		))
			
		set @koszt = (@il_os_z_leg * (@koszt_warsztatu * (1 - @znizka)) + @il_os_bez_leg * @koszt_warsztatu)

		return @koszt
	end
go


-- =============================================
-- Author:		PiotrWisniewski
-- Description:	koszt_dniaKonferencji
--zwraca koszt rezerwacji dnia konferencji
--(parametry: id_rerwacji
--koszt: il_os_z_leg*(oplata_dk * 1-znizka) + il_os_bez_leg * oplata_dk)

-- =============================================

GO
create function koszt_dniaKonferencji(@id_rezKonferencja int) 
	returns money
	as
	begin
		declare @koszt as money
		declare @oplata_dk as money
		declare @il_os_z_leg as int
		declare @il_os_bez_leg as int
		declare	@znizka as float 
		declare @dataRezerwacji as date
		declare @najwczesniejszyProg as date

		set @dataRezerwacji = (select dataRezerwacji from RezerwacjaKonferencji where id_rezKonferencja = @id_rezKonferencja)

		set @najwczesniejszyProg = (
			select MIN(poczatekProgu) from Oplaty where id_konferencja in (
				select id_konferencja from Konferencja where id_konferencja = (
					select id_konferencja from DzienKonferencji where id_dk =(
						select id_DK from RezerwacjaKonferencji where id_rezKonferencja = @id_rezKonferencja
					)
				)
			)
		)
		if (@najwczesniejszyProg > @dataRezerwacji)
		begin
			set @oplata_dk = (
				select cena from Oplaty as o where id_konferencja in (
					select id_konferencja from Konferencja where id_konferencja = (
						select id_konferencja from DzienKonferencji where id_dk =(
							select id_DK from RezerwacjaKonferencji where id_rezKonferencja =  @id_rezKonferencja
						)
					)
				
				and o.poczatekProgu = @najwczesniejszyProg
				)
			)
		end
		else
		begin
		set @oplata_dk =(
			select cena from Oplaty as o where id_konferencja in (
				select id_konferencja from Konferencja where id_konferencja = (
					select id_konferencja from DzienKonferencji where id_dk =(
						select id_DK from RezerwacjaKonferencji where id_rezKonferencja =  @id_rezKonferencja
					)
				)
				and (@dataRezerwacji >= o.poczatekProgu and @dataRezerwacji <= o.koniecProgu)  
			))
		end
		set @il_os_z_leg = (
			
			select Count(*) from RejestracjaKonferencji where id_rezKonferencja =  @id_rezKonferencja
													   and legitymacja is not null 
			)
		set @il_os_bez_leg = (
			
			select Count(*) from RejestracjaKonferencji where id_rezKonferencja = @id_rezKonferencja
													   and legitymacja is null 
			)
		
		set @znizka = (
			select znizka from DzienKonferencji where id_dk = (
				select id_DK from RezerwacjaKonferencji where id_rezKonferencja =  @id_rezKonferencja
		))
			
		set @koszt = (@il_os_z_leg * (@oplata_dk * (1 - @znizka)) + @il_os_bez_leg * @oplata_dk)
		
		return @koszt
	end
go

-- =============================================
-- Author:		Piotr Wisniewski
-- Description: lista_najaktywniejszych_klientow(najwiecej rejestracji ich uczestnikow)
-- =============================================

GO
create procedure lista_najaktywniejszych_klientow
	
	as		
		select K.id_klienta, sum(Kon.ilosc_miejsc) + sum(War.ilosc_miejsc) as Suma, 
			sum(Kon.ilosc_miejsc) as rezKonferencji, sum(War.ilosc_miejsc) as rezWarsztatow
			from Klienci as K
			inner join RezerwacjaKonferencji as Kon on Kon.id_klienta = K.id_klienta
			inner join RezerwacjaWarsztat as War on War.id_klienta = K.id_klienta
			group by K.id_klienta
			order by 2 desc
		
	go




-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	liczba_wolnych_miejsc_na_warsztat
/*
zwraca liczbe wolnych miejsc na ten warsztat,
od liczby_miejsc odejmuje zarezerwowane miejsca przez klientow
*/
-- =============================================


GO
create function liczba_wolnych_miejsc_na_warsztat(@id_warsztat int) 
	returns smallint
	as
	begin
		declare @liczba_miejsc as smallint 
		declare @zarezerwowane as smallint 
		
		set @liczba_miejsc =  
		(
			select liczba_miejsc from Warsztat 
			where id_warsztat = @id_warsztat
		)
		
		set @zarezerwowane = 
		(
			select SUM(ilosc_miejsc) from RezerwacjaWarsztat where id_w = @id_warsztat
			and anulowano = 0
		)

		return @liczba_miejsc - @zarezerwowane 
		
	end
go

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	liczba_wolnych_miejsc_na_dk
/*
zwraca liczbe wolnych miejsc na ten dzien konferencji,
od liczby_miejsc odejmuje zarezerwowane miejsca przez klientow
*/
-- =============================================


GO
create function liczba_wolnych_miejsc_na_dk(@id_dk int) 
	returns smallint
	as
	begin
		declare @liczba_miejsc as smallint 
		declare @zarezerwowane as smallint 
		
		set @liczba_miejsc =  
		(
			select liczba_miejsc from DzienKonferencji 
			where id_dk = @id_dk
		)
		
		set @zarezerwowane = 
		(
			select SUM(ilosc_miejsc) from RezerwacjaKonferencji where id_DK = @id_dk
		    and anulowano = 0
		)

		return @liczba_miejsc - @zarezerwowane 
		
	end
go

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	liczba_wolnych_miejsc_na_rezW
/*
*/
-- =============================================


GO
create function liczba_wolnych_miejsc_na_rezW(@id_rezWarsztat int) 
	returns smallint
	as
	begin
		declare @ilosc_potwierdzen as smallint 
		declare @ilosc_miejsc as smallint 
		
		set @ilosc_potwierdzen =  
		(
			select ilosc_potwierdzen from RezerwacjaWarsztat where id_rezWarsztat = @id_rezWarsztat
		)
		
		set @ilosc_miejsc = 
		(
			select ilosc_miejsc from RezerwacjaWarsztat where id_rezWarsztat = @id_rezWarsztat
		)

		return @ilosc_miejsc - @ilosc_potwierdzen
		
	end
go

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	liczba_wolnych_miejsc_na_rezDK
/*
*/
-- =============================================


GO
create function liczba_wolnych_miejsc_na_rezDK(@id_rezKonferencja int) 
	returns smallint
	as
	begin
		declare @ilosc_potwierdzen as smallint 
		declare @ilosc_miejsc as smallint 
		
		set @ilosc_potwierdzen =  
		(
			select ilosc_potwierdzen from RezerwacjaKonferencji where id_rezKonferencja = @id_rezKonferencja
		)
		
		set @ilosc_miejsc = 
		(
			select ilosc_miejsc from RezerwacjaKonferencji where id_rezKonferencja = @id_rezKonferencja
		)

		return @ilosc_miejsc - @ilosc_potwierdzen
		
	end
go

-- =============================================
-- Author:		Piotr Wisniewski
-- Description: kolizja Warsztatu 
/*
zwraca 0 jesli nie ma kolizji (nie ma tabeli, gdzie (w.godzR < godzR && w.godzZ > godz R)||(w.godzZ > godzZ && w.godzR < godzZ))
zraca 1 w przeciwnym wypadku
*/
-- =============================================

create function kolizjaWarsztatu(@id_uczestnika varchar(10), @id_warsztat int)
	returns smallint
	as
	begin
		declare @godzR  smalldatetime
		declare @godzZ  smalldatetime
		declare @bit smallint
		
		set @godzR = (select godz_rozpoczecia from Warsztat where id_warsztat = @id_warsztat)
		set @godzZ = (select godz_zakonczenia from Warsztat where id_warsztat = @id_warsztat)
		
		IF exists (select * from Warsztat as w
						   inner join RezerwacjaWarsztat as rez on rez.id_w = w.id_warsztat
						   inner join RejestracjaWarsztat as rej on rej.id_rezWarsztat = rez.id_rezWarsztat
						   where rej.id_uczestnika = @id_uczestnika
						   and  w.id_dk = (select id_dk from Warsztat where id_warsztat = @id_warsztat)
						   and 
						   (((w.godz_rozpoczecia < @godzR) and (w.godz_zakonczenia > @godzR))
						   or ((w.godz_zakonczenia > @godzZ) and (w.godz_rozpoczecia < @godzZ))))
		begin
			set @bit = 1
		end
		else 
		begin
			set @bit = 0
		end
		return @bit
	end
go


-- =============================================
-- Author:		Piotr Wisniewski
-- Description: telefonySpoznialskich
/*
zwraca telefony tych klientow, ktorzy nie wypelnili listy 2 tygodnie przed konferencja
*/
-- =============================================

create view telefonySpoznialskich
	as
		select kli.id_klienta, kli.telefon from Klienci as kli
					   inner join RezerwacjaKonferencji as rk on rk.id_klienta=kli.id_klienta
					   inner join DzienKonferencji as Dkon on Dkon.id_konferencja = rk.id_DK
					   inner join Konferencja as kon on kon.id_konferencja = Dkon.id_konferencja
					   where DATEDIFF(d, rk.dataRezerwacji ,kon.dataRozpoczecie)<14
					   and rk.ilosc_miejsc>rk.ilosc_potwierdzen
					   and rk.anulowano=0
					   and kon.dataRozpoczecie > GETDATE()






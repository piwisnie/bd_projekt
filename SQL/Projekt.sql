--drop database Konferencje
go
create database Konferencje
go
use	Konferencje
go

create table Klienci(
	id_klienta int not null primary key identity (1,1),
	telefon varchar(50) not null
)

create table Firma(
	id_firmy int not null primary key identity (1,1),
	id_klienta int unique not null foreign key references Klienci(id_klienta),
	nazwa varchar(50),
	NIP int unique not null
)

create table Osoby(
	id_osoby int not null primary key identity (1,1),
	id_klienta int unique not null foreign key references Klienci(id_klienta),
	imie varchar(50),
	nazwisko varchar(50),
	PESEL varchar (11) unique not null
)

create table Uczestnicy(
	id_uczestnika varchar(10) not null primary key,
	imie varchar(50),
	nazwisko varchar(50),
	PESEL varchar(11) unique not null,
	legitymacja varchar(20) null
)

create table Konferencja(
	id_konferencja int not null primary key identity (1,1),
	nazwa varchar(50) not null,
	dataRozpoczecie date not null,
	dataZakonczenia date not null 
)


create table DzienKonferencji(
	id_dk int not null primary key identity (1,1),
	id_konferencja int not null foreign key references Konferencja(id_konferencja),
	godz_rozpoczecia smalldatetime not null,
	godz_zakonczenia smalldatetime not null,
	liczba_miejsc int not null check(liczba_miejsc>0),
	znizka float null check ((znizka between 0.0 and  1.0)  or znizka = null)
)

create table Warsztat(
	id_warsztat int not null primary key identity (1,1),
	id_dk int not null foreign key references DzienKonferencji(id_dk),
	godz_rozpoczecia smalldatetime not null,
	godz_zakonczenia smalldatetime not null,
	liczba_miejsc int not null check(liczba_miejsc>0),
	cena money check(cena>=0),
	znizka float null check ((znizka between 0.0 and  1.0)  or znizka = null)
)

create table RezerwacjaKonferencji(
	id_rezKonferencja int not null primary key identity (1,1),
	id_klienta int not null foreign key references Klienci(id_klienta),
	ilosc_miejsc int not null check(ilosc_miejsc>=0),
	ilosc_potwierdzen int default 0,
	id_DK int not null references DzienKonferencji(id_DK),
	dataRezerwacji date not null,
	anulowano bit not null default 0,
	dataAnulowania date null

)

create table RezerwacjaWarsztat(
	id_rezWarsztat int not null primary key identity (1,1),
	id_klienta int not null foreign key references Klienci(id_klienta),
	ilosc_miejsc int not null check(ilosc_miejsc>=0),
	ilosc_potwierdzen int default 0,
	id_w int not null references Warsztat(id_warsztat),
	dataRezerwacji date not null,
	anulowano bit not null default 0,
	dataAnulowania date null
)

create table RejestracjaKonferencji(
	id_rejKonferencja int not null primary key identity (1,1),
	id_uczestnika varchar(10) not null foreign key references Uczestnicy(id_uczestnika),
	id_rezKonferencja int not null foreign key references RezerwacjaKonferencji(id_rezKonferencja),
	legitymacja varchar(20) null
)

create table RejestracjaWarsztat(
	id_rejWarsztat int not null primary key identity (1,1),
	id_uczestnika varchar(10) not null foreign key references Uczestnicy(id_uczestnika),
	id_rezWarsztat int not null foreign key references RezerwacjaWarsztat(id_rezWarsztat),
	legitymacja varchar(20) null
)

create table Oplaty(
	id_oplaty int not null primary key identity (1,1),
	id_konferencja int not null foreign key references Konferencja(id_konferencja),
	poczatekProgu date not null,
	koniecProgu date not null,
	cena money not null check(cena>=0)
)



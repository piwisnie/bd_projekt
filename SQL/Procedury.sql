use Konferencje

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj klienta
-- =============================================
GO

CREATE PROCEDURE dodaj_klienta
	-- parametry
	@Telefon nvarchar(50)

AS
BEGIN
	SET NOCOUNT ON;
		INSERT INTO Klienci 
		values(@Telefon)
END


-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj firme
-- =============================================

GO

CREATE PROCEDURE dodaj_firme
	-- parametry
	@nazwa nvarchar(50)=NULL,
	@NIP int,
	@Telefon varchar(50)

AS
BEGIN
	SET NOCOUNT ON;
	declare @id_klienta  as int;
	begin try

		--dodaj nowego klienta
		execute dodaj_klienta @Telefon;
		set @id_klienta = @@IDENTITY;
		
		-- dodaj now� firm�
		if(select id_klienta from Firma where NIP = @NIP) is NULL
		begin 
			INSERT INTO Firma
			VALUES(@id_klienta,@nazwa, @NIP);
		end
	end try
	begin catch
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� firmy, b��d danych. %s', 16, 1,@error);	
	end catch
END
GO

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj osobe
-- =============================================

GO

CREATE PROCEDURE dodaj_osobe
	-- parametry
	@imie nvarchar(50)=NULL,
	@nazwisko nvarchar(50)=NULL,
	@PESEL varchar(11),
	@Telefon varchar(50)

AS
BEGIN
	SET NOCOUNT ON;
	declare @id_klienta  as int;
	begin try

		--dodaj nowego klienta
		execute dodaj_klienta @Telefon;
		set @id_klienta = @@IDENTITY;
		
		-- dodaj now� osob�
		begin
			INSERT INTO Osoby
			VALUES(@id_klienta,@imie, @nazwisko, @PESEL);
		end
	end try
	begin catch
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� osoby, b��d danych. %s', 16, 1,@error);	
	end catch
END
GO

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj uczestnika
-- =============================================

GO

CREATE PROCEDURE dodaj_uczestnika
	-- parametry
	@id_uczestnika nvarchar(10),
	@imie nvarchar(50)=NULL,
	@nazwisko nvarchar(50)=NULL,
	@PESEL varchar(11),
	@legitymacja varchar(20)=NULL

AS
BEGIN
	SET NOCOUNT ON;
	
		
	-- dodaj nowego uczesntika
	if (select id_uczestnika from Uczestnicy where id_uczestnika = @id_uczestnika) is null
	begin
		INSERT INTO Uczestnicy
		VALUES(@id_uczestnika, @imie, @nazwisko, @PESEL, @legitymacja);
	end
	else
	begin 
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� uczesntika, b��d danych. %s', 16, 1,@error);	
	end 
END
GO

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj konferencje
-- =============================================

GO

CREATE PROCEDURE dodaj_konferencje
	-- parametry
	@nazwa nvarchar(50),
	@dataRozpoczecia date,
	@dataZakonczenia date

AS
BEGIN
	SET NOCOUNT ON;
	begin try
		
		-- dodaj nowa konferencje
		INSERT INTO Konferencja
		VALUES(@nazwa, @dataRozpoczecia, @dataZakonczenia);
	end try
	begin catch
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� konferencji, b��d danych. %s', 16, 1,@error);	
	end catch
END
GO

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj oplaty
-- =============================================

GO

CREATE PROCEDURE dodaj_oplate
	-- parametry
	@id_konferencja int,
	@poczatekProgu date,
	@koniecProgu date,
	@cena money

AS
BEGIN
	SET NOCOUNT ON;
	begin try
		
		-- dodaj nowa oplate
		INSERT INTO Oplaty
		VALUES(@id_konferencja, @poczatekProgu, @koniecProgu, @cena);
	end try
	begin catch
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� oplaty, b��d danych. %s', 16, 1,@error);	
	end catch
END
GO

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj dzien konferencji
-- =============================================

GO
CREATE PROCEDURE dodaj_dzienKonferencji
	-- parametry
	@id_konferencja int,
	@godz_rozpoczecia smalldatetime,
	@godz_zakonczenia smalldatetime,
	@liczba_miejsc int,
	@znizka float = null

AS
BEGIN
	SET NOCOUNT ON
	
	if(@godz_rozpoczecia >= @godz_zakonczenia)
	begin
		RAISERROR('Nie mozna dodac konferencji, ktorej godzRozpoczecia >= godzZakonczenia' , 16 , 1)
		RETURN
	end
	else
	if(@id_konferencja in (select id_konferencja from Konferencja))
	begin
		-- dodaj nowy dzien konferencji
		INSERT INTO DzienKonferencji
		VALUES(@id_konferencja,	@godz_rozpoczecia, @godz_zakonczenia, @liczba_miejsc, @znizka);
	end
	else
	begin
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� dnia konferencji, nie ma takiej konferencji. %s', 16, 1,@error);	
	end
END
GO

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj warsztat
-- =============================================

GO
CREATE PROCEDURE dodaj_warsztat
	-- parametry
	@id_dk int,
	@godz_rozpoczecia smalldatetime,
	@godz_zakonczenia smalldatetime,
	@liczba_miejsc int,
	@cena money,
	@znizka float =null

AS
BEGIN
	SET NOCOUNT ON
	if(@godz_rozpoczecia >= @godz_zakonczenia)
	begin
		RAISERROR('Nie mozna dodac warsztatu, ktorej godzRozpoczecia >= godzZakonczenia' , 16 , 1)
		RETURN
	end
	else
	if(@id_dk in (select id_dk from DzienKonferencji))
	begin
		-- dodaj nowy warsztat
		INSERT INTO Warsztat
		VALUES(@id_dk,	@godz_rozpoczecia, @godz_zakonczenia, @liczba_miejsc, @cena, @znizka);
	end
	else
	begin
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� warsztatu, nie istnieje taki dzien konferencji. %s', 16, 1,@error);	
	end 
END
GO

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj rezKonferencja
-- =============================================

GO
CREATE PROCEDURE dodaj_rezKonferencja
	-- parametry
	@id_klienta int,
	@ilosc_miejsc int,
	@ilosc_potwierdzen int,
	@id_DK int,
	@dataRezerwacji date,
	@anulowano bit = 0,
	@dataAnulowania date = NULL

AS
BEGIN
	SET NOCOUNT ON
	if(@id_klienta in (select id_klienta from Klienci) and @id_DK in (select id_DK from DzienKonferencji))
	begin
		-- dodaj nowa rezerwacje na Konferencje
		INSERT INTO RezerwacjaKonferencji
		VALUES(@id_klienta, @ilosc_miejsc, @ilosc_potwierdzen, @id_DK, @dataRezerwacji, @anulowano, @dataAnulowania);
	end
	else
	begin
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� rezerwacji konferencji, taki klient albo DK nie istnieje. %s', 16, 1,@error);	
	end 
END


-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj rejKonferencja
-- =============================================

GO
CREATE PROCEDURE dodaj_rejKonferencja
	-- parametry
	@id_uczestnika nvarchar(10),
	@id_rezKonferencja int,
	@legitymacja nvarchar(20) = NULL

AS
BEGIN
	SET NOCOUNT ON
	if(@id_uczestnika in (select id_uczestnika from Uczestnicy) and 
	   @id_rezKonferencja in (select id_rezKonferencja from RezerwacjaKonferencji))
	begin
		
		-- dodaj nowa rejestracje na Konferencje
		INSERT INTO RejestracjaKonferencji
		VALUES(@id_uczestnika, @id_rezKonferencja, @legitymacja);
		update RezerwacjaKonferencji
		SET ilosc_potwierdzen += 1 where id_rezKonferencja = @id_rezKonferencja 
	end 
	else
	begin 
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� rejestracji konferencji, nie ma takiego uczestnika lub rezK. %s', 16, 1,@error);	
	end 
END


-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj rezWarsztat
-- =============================================

GO
CREATE PROCEDURE dodaj_rezWarsztat
	-- parametry
	@id_klienta int,
	@ilosc_miejsc int,
	@ilosc_potwierdzen int,
	@id_w int,
	@data_rezerwacji date,
	@anulowano bit = 0,
	@dataAnulowania date = NULL

AS
BEGIN
	SET NOCOUNT ON
	if(@id_klienta in (select id_klienta from Klienci) and @id_w in (select id_warsztat from Warsztat))
	begin
		
		-- dodaj nowa rezerwzcje na Warsztat
		INSERT INTO RezerwacjaWarsztat
		VALUES(@id_klienta, @ilosc_miejsc, @ilosc_potwierdzen, @id_w, @data_rezerwacji, @anulowano, @dataAnulowania);
	end 
	else
	begin
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� rezerwacji warsztatu, brak klienta lub W. %s', 16, 1,@error);	
	end 
END


-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	dodaj rejWarsztat
-- =============================================

GO
CREATE PROCEDURE dodaj_rejWarsztat
	-- parametry
	@id_uczestnika nvarchar(10),
	@id_rezWarsztat int,
	@legitymacja nvarchar(20)

AS
BEGIN
	SET NOCOUNT ON
	if(@id_uczestnika in (select id_uczestnika from Uczestnicy) and 
	   @id_rezWarsztat in (select id_rezWarsztat from RezerwacjaWarsztat))
	begin 
		-- dodaj nowa rejestracje na Warsztat
		INSERT INTO RejestracjaWarsztat
		VALUES(@id_uczestnika, @id_rezWarsztat, @legitymacja);
		update RezerwacjaWarsztat
		SET ilosc_potwierdzen += 1 where id_rezWarsztat = @id_rezWarsztat
	end 
	else 
	begin 
		declare @error as varchar(127)
		set @error = (Select ERROR_MESSAGE() as error_message)
		RAISERROR('Nie mo�na doda� rejestracji warsztatu, b��d danych. %s', 16, 1,@error);	
	end 
END

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	anuluj_rezerwacje_konferencji
-- =============================================
GO
CREATE PROCEDURE deaktywuj_rezerwacje_konferencji
	-- parametry
	@id_rezKonferencja int
	
AS
BEGIN
	SET NOCOUNT ON;
		declare @anulowano as bit 
		set @anulowano = 
		(
			select anulowano from RezerwacjaKonferencji
			where id_rezKonferencja = @id_rezKonferencja
		)
		IF(@anulowano = 0)
		BEGIN
			UPDATE RezerwacjaKonferencji
			SET	anulowano = 1
			WHERE id_rezKonferencja = @id_rezKonferencja
		END
		ELSE IF (@anulowano = 1)
		BEGIN
				RAISERROR('Nie mozna anulowac konferencji, juz jest anulowana', 16, 1);
		END
		ELSE
				RAISERROR('Nie mozna deaktywowac konferencji, bo nie istnieje!', 16, 1);

END
GO

-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	anuluj_rezerwacje_warsztatu
-- =============================================
GO
CREATE PROCEDURE deaktywuj_rezerwacje_warsztatu
	-- parametry
	@id_rezWarsztat int
	
AS
BEGIN
	SET NOCOUNT ON;
		declare @anulowano as bit 
		set @anulowano = 
		(
			select anulowano from RezerwacjaWarsztat
			where id_rezWarsztat = @id_rezWarsztat
		)
		IF(@anulowano = 0)
		BEGIN
			UPDATE RezerwacjaWarsztat
			SET	anulowano = 1
			WHERE id_rezWarsztat = @id_rezWarsztat		
		END
		ELSE IF (@anulowano = 1)
		BEGIN
				RAISERROR('Nie mozna anulowac warsztatu, juz jest anulowany', 16, 1);
		END
		ELSE
				RAISERROR('Nie mozna deaktywowac warsztatu, bo nie istnieje!', 16, 1);

END
GO


-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	anuluj_nieoplacone_rezerwacje na konferencje
-- =============================================
GO

CREATE PROCEDURE anuluj_nieoplacone_rezerwacje_na_konferencje

AS
BEGIN

	SET NOCOUNT ON;
	update RezerwacjaKonferencji 
	set anulowano = 1,
	dataAnulowania = GETDATE()
	from RezerwacjaKonferencji as rk 
	inner join DzienKonferencji as dk on dk.id_dk = rk.id_DK
	inner join Konferencja as k on k.id_konferencja = dk.id_konferencja
	where ( ( DATEDIFF(d,rk.DataRezerwacji, GETDATE()) > 7)
	and rk.ilosc_miejsc > rk.ilosc_potwierdzen
	and rk.anulowano = 0
	and k.dataRozpoczecie > GETDATE())
END
GO


-- =============================================
-- Author:		Piotr Wisniewski
-- Description:	anuluj_nieoplacone_rezerwacje na warsztaty
-- =============================================
GO

CREATE PROCEDURE anuluj_nieoplacone_rezerwacje_na_warsztaty

AS
BEGIN

	SET NOCOUNT ON;
	update RezerwacjaWarsztat
	set anulowano = 1,
	dataAnulowania = GETDATE()
	from RezerwacjaWarsztat as rw 
	inner join Warsztat as w on w.id_warsztat = rw.id_w
	where ( ( DATEDIFF(d,rw.DataRezerwacji, GETDATE()) > 7)
	and rw.ilosc_miejsc > rw.ilosc_potwierdzen
	and rw.anulowano = 0
	and w.godz_rozpoczecia > GETDATE())
END
GO



